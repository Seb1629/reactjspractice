
var GreeterMessage = React.createClass({
  render: function(){
    return (
      <div>
        <h1>Some H1</h1>
        <p>Some text</p>
      </div>

    );
  }
});


var GreeterForm = React.createClass({
  render: function(){
    return(
      <div>
        <form>
          <input type="text" ref="name"/>
          <button>Add Text</button>
        </form>
      </div>
    );
  }
});


var Greeter = React.createClass({

getDefaultProps: function(){
  return {
    name: 'Mister',
    message: 'Welcome to our fancy website'
  };
},

// AWESOMENESS
// takes the initial state of the value name which is in getDefaultProps
getInitialState: function(){
  return {
    name: this.props.name
  };
},

onButtonClicked: function(e) {
      e.preventDefault();
      var nameRef = this.refs.name;
      var name = nameRef.value;
        nameRef.value = '';

        if (typeof name === 'string' && name.length > 0) {
          this.setState({
            name: name
          });
        }
},

  render: function(){
  var name = this.state.name; // using state allow to change the value of the component
  var message = this.props.message;

    return (
      <div>
        <h1>Hey {name}!</h1>
        <p1>{message}</p1>
        <GreeterMessage/>
        <GreeterForm/>

        {/* <form onSubmit={this.onButtonClicked}>
          <input type="text" ref="name"/>
          <button>Add Name</button>
        </form> */}
      </div>

    );
  }
});

var name = "enter your name!"

// to initialize the component
ReactDOM.render(
<Greeter name = {name}/>,document.getElementById('app')
);
